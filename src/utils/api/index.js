import studentAuth from './modules/studentAuth';
import course from './modules/course';
import student from './modules/student';

export default (api) => ({
    studentAuth: studentAuth(api),
    course: course(api),
    student: student(api),
})