export default (api) => ({
    courseProgress(id) {
        return api.request.get(`/student/courses/${id}/progress`);
    },
    completeLesson(courseId, blockNum, lessonNum) {
        return api.request.post(`/student/courses/${courseId}/${blockNum}/${lessonNum}/complete`);
    },
    checkTestLesson(courseId, blockNum, lessonNum, answers) {
        return api.request.post(`/student/courses/${courseId}/${blockNum}/${lessonNum}/test`, { answers });
    }
});