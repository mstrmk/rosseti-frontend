export default (api) => ({
    fetchAll() {
        return api.request.get('/student/courses');
    },
    fetchOne(id) {
        return api.request.get(`/student/courses/${id}`);
    },
    getLesson(courseId, blockNum, lessonNum) {
        return api.request.get(`/student/courses/${courseId}/${blockNum}/${lessonNum}`);
    },
    setFlagViewLesson(courseId, blockNum, lessonNum) {
        return api.request.post(`/student/courses/${courseId}/${blockNum}/${lessonNum}/view`);
    },
});