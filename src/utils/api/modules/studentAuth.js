import fp from '@/utils/helpers/fingerprint';

const authPath = 'auth/student';

export default (api) => ({
    async login(login, password) {
        const fingerprint = await fp();
        return api.request.post(authPath + '/login', { login, password, fingerprint });
    },
    me() {
        return api.request.get(authPath + '/me');
    },
    async refresh(refreshToken) {
        const fingerprint = await fp();
        return api.request.post(authPath + '/refresh', { refreshToken, fingerprint });
    },
    logout(refreshToken) {
        return api.request.post(authPath + '/logout', { refreshToken }).send();
    },
});