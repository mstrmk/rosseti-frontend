const qs = require('qs');

const with__call = ($this) => {
    return new Proxy($this, {
        get: function($this, field) {
            if (field == 'then') {
                return with__call($this);
            }

            if (field in $this) {
                return $this[field];
            }

            return (...args) => {
                return $this.on(field, args[0]);
            }
        }
    });
}

const STATUS_CODES = {
    ok: 200,
    created: 201,
    unauthorized: 401,
    forbidden: 403,
    notFound: 404,
    invalidate: 422,
    serverError: 500,
    catch: 0,
}

class ApiRequest {

    constructor(connect, defaultHandlers = {}) {
        this._connect = connect;

        const $this = with__call(this);

        for (let status in defaultHandlers) {
            $this[status](defaultHandlers[status]);
        }

        return $this;
    }

    _handlers = {
        200: (data) => {
            return data;
        }
    }

    _flagLoading = null;

    _default = {
        data: null,
        message: null
    };

    _method = 'get';
    _url = {};
    _data = {};
    _config = {};

    on(status, cb) {
        if (STATUS_CODES[status] === undefined) {
            throw `Error set handler [${status}] - not found`;
        }

        this._handlers[STATUS_CODES[status]] = cb;

        return with__call(this);
    }

    off(status) {
        if (STATUS_CODES[status] === undefined) {
            throw `Error off handler [${status}] - not found`;
        }

        delete(this._handlers[STATUS_CODES[status]]);

        return with__call(this);
    }

    default (data = null, message = null) {
        this._default = {
            data,
            message
        };

        return with__call(this);
    }

    get(url, params = {}, config = {}) {
        // this._params = {
        //     method: 'get',
        //     url: url + '?' + qs.stringify(params),
        //     ...config
        // };
        this._method = 'get';
        this._url = url;
        this._data = params;
        this._config = config;

        return this;
    }

    create(method, url, data, config = {}) {
        // this._params = {
        //     method,
        //     url,
        //     data,
        //     ...config
        // };

        this._method = method;
        this._url = url;
        this._data = data;
        this._config = config;

        return this;
    }

    post(url, data, config = {}) {
        return this.create('post', url, data, config);
    }

    put(url, data, config = {}) {
        return this.create('put', url, data, config);
    }

    delete(url, data, config = {}) {
        return this.create('delete', url, data, config);
    }

    setData(data) {
        this._data = data;
        return this;
    }

    flagLoading(component, property) {
        this._flagLoading = {
            component,
            property
        };
        return this;
    }

    setLoading(loading) {
        if (this._flagLoading) {
            this._flagLoading.component[this._flagLoading.property] = loading;
        }
    }

    setProcess(isProcess) {
        this._isProcess = isProcess;
        this.setLoading(isProcess);
        if (isProcess) {
            this._canceled = false;
        }
    }

    isProcess() {
        return this._isProcess;
    }

    _cancelSource = null;
    _canceled = false;
    _isProcess = false;

    cancel() {
        if (this._cancelSource) {
            this._cancelSource.cancel();
        }
        this.setLoading(false);
        this._canceled = true;
    }

    async send(cancelPrev = true) {
        try {
            if (cancelPrev) {
                this.cancel();
            }

            this.setProcess(true);

            const params = {
                method: this._method,
                url: this._url,
                ...this._config
            };

            if (this._method != 'get') {
                params.data = this._data;
            } else {
                params.url = params.url + '?' + qs.stringify(this._data);
            }

            const source = this._connect.CancelToken.source();
            this._cancelSource = source;
            params.cancelToken = this._cancelSource.token;

            const response = await this._connect(params);

            const result = this._handlers[response.status] && !this._canceled ? await this._handlers[response.status](response.data.data, response.data.message, this) : null;

            this.setProcess(false);

            return result !== null ? result : this._default;
        } catch (e) {
            if (this._connect.isCancel(e)) {
                this._isProcess = false;
                return this._default;
            }

            const response = e.response;
            if (response) {
                let result = null;
                if (this._handlers[response.status]) {
                    result = await this._handlers[response.status](response.data.data, response.data.message, this);
                } else if (this._handlers[0]) {
                    result = await this._handlers[0](response.data.data, response.data.message, this, response.status);
                }

                this.setProcess(false);

                return result ? result : { data: this._default, message: null };
            }

            this.setProcess(false);
            // throw e;
        }
    }

}

export default ApiRequest;