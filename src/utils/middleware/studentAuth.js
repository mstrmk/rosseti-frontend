import store from '@/store'

export default {
    async check(to, from, next) {
        if (!store.getters['studentAuth/isAuth'] && !store.getters['studentAuth/student'] && store.getters['studentAuth/accessToken']) {
            await store.dispatch('studentAuth/me');
        }

        next();
    },
    async authenticated(to, from, next) {
        if (!store.getters['studentAuth/isAuth'] && (!store.getters['studentAuth/accessToken'] || !await store.dispatch('studentAuth/me'))) {
            next({ name: 'login' });
            return;
        }

        next();
    },
    async notAuthenticated(to, from, next) {
        if (store.getters['studentAuth/isAuth'] || (store.getters['studentAuth/accessToken'] && await store.dispatch('studentAuth/me'))) {
            next("/");
            return;
        }

        next();
    }
};