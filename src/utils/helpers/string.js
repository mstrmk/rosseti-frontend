export default {
    phone: {
        format(phone) {
            return `+7 (${phone.substr(0, 3)}) ${phone.substr(3, 3)}-${phone.substr(6, 2)}-${phone.substr(8, 2)}`;
        },
        deformat(phone) {
            let result = phone;
            if (result.substr(0, 2) == '+7') {
                result = result.substr(2);
            }
            return result.replaceAll(/\D+/g, '');
        },
        valid(phone) {
            return this.deformat(phone).length == 10;
        }
    },

    bankCard: {
        format(number) {
            return [number.substr(0, 4), number.substr(4, 4), number.substr(8, 4), number.substr(12, 4)].join(' ');
        }
    }
};