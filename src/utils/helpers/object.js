export default {
    assignDeep(...args) {
        let result = args[0];
        for (let i = 1; i < args.length; i++) {
            let obj = args[i];
            for (const key of Object.keys(obj)) {
                if (obj[key] instanceof Object)
                    Object.assign(obj[key], this.assignDeep(result[key], obj[key]))
            }

            result = Object.assign(result, obj);
        }
        return result;
    },
    clone(obj) {
        return JSON.parse(JSON.stringify(obj));
    },
    clearDeep(obj, clearValues = []) {
        let result = {};

        for (let key in obj) {
            const prop = obj[key];
            if (Array.isArray(prop) && prop.length) {
                result[key] = prop;
            } else if (typeof prop == 'object') {
                let propObj = this.clearDeep(prop, clearValues);
                if (Object.keys(propObj).length > 0) {
                    result[key] = propObj;
                }
            } else if (prop !== null && !clearValues.includes(prop)) {
                result[key] = prop;
            }
        }

        return result;
    },
    isEmpty(obj) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }
}