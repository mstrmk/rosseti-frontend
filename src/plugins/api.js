import Vue from 'vue';
import store from '@/store';
import router from '@/router';

import Api from '@/utils/api/core/Api';

import apiProvider from "@/utils/api";

export default {
    async install({ $axios }) {
        $axios.defaults.baseURL = process.env.VUE_APP_API_DOMAIN ? process.env.VUE_APP_API_DOMAIN.trim('/') + '/' : 'api.localho.st/';

        const unAuthCallback = async(data, message, request) => {
            if (!await store.dispatch('studentAuth/refresh')) {
                router.push({name: "login"});
            } else {
                request.unauthorized((data, message) => {
                    router.push({name: "login"});
                }).send();

                request.unauthorized(unAuthCallback);
            }
        };

        const api = new Api($axios, {
            unauthorized: unAuthCallback,
        });

        api.setModules(apiProvider(api));

        Vue.prototype.$api = api;
    }
}