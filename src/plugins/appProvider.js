import Api from "./api";
import Helpers from "./helpers";

import vSelect from 'vue-select'
import "vue-select/src/scss/vue-select.scss";

import VueMoment from 'vue-moment'

import { VBTooltipPlugin, VBPopoverPlugin } from 'bootstrap-vue'

import VueFlatPickr from 'vue-flatpickr-component';
import 'flatpickr/dist/flatpickr.css';

import constants from "@/utils/constants";

import "@/assets/scss/app.scss";

export default {
    install(Vue) {
        Vue.use(Helpers);
        Vue.use(Api);
        Vue.use(VueMoment);
        Vue.use(VueFlatPickr, "FlatPicker");
        Vue.use(VBTooltipPlugin);
        Vue.use(VBPopoverPlugin);

        Vue.component('v-select', vSelect);

        Vue.prototype.$constants = constants;

    }
};