import string from '@/utils/helpers/string';
import object from '@/utils/helpers/object';
import fingerprint from '@/utils/helpers/fingerprint';

export default {
    async install(Vue) {
        Vue.prototype.$helpers = {
            string,
            object,
            fingerprint
        };
    }
}