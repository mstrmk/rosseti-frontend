import Vue from "vue";
import VueHead from 'vue-head'

import router from "./router";
import store from './store';
import App from "./App.vue";


import Argon from "./plugins/argon-kit";
import './registerServiceWorker'

import AppProvider from "./plugins/appProvider";

import axios from 'axios'
import VueAxios from 'vue-axios'

import VueMask from 'v-mask'

Vue.config.productionTip = false;
Vue.use(Argon);

Vue.use(VueAxios, { $axios: axios });
Vue.use(VueMask);

Vue.use(AppProvider);

Vue.use(VueHead, {
    separator: ' - ',
    complement: 'Rosseti',
});

new Vue({
    store,
    router,
    render: h => h(App)
}).$mount("#app");