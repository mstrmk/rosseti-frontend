export default {

    state: () => ({
        all: [],
    }),

    getters: {
        all: state => state.all,
    },

    actions: {
        async fetchAll({ commit }) {
            await this._vm.$api.course.fetchAll()
                .ok((data, message) => {
                    commit('setAll', data);
                })
                .send();
        }
    },

    mutations: {
        setAll(state, courses) {
            state.all = courses;
        },
    },

    namespaced: true,

}