const ACCESS_TOKEN_NAME = 'studentAccessToken';
const REFRESH_TOKEN_NAME = 'studentRefreshToken';

export default {

    state: () => ({
        student: null,
        accessToken: localStorage.getItem(ACCESS_TOKEN_NAME) || '',
        authStatus: false,
    }),

    getters: {
        student: state => state.student,
        accessToken: state => state.accessToken,
        isAuth: state => !!state.student && !!state.accessToken,
        authStatus: state => state.authStatus,
    },

    actions: {
        async me({ commit, state, dispatch }) {
            this._vm.$axios.defaults.headers.common['Authorization'] = `Bearer ${state.accessToken}`;

            let result = false;

            await this._vm.$api.studentAuth.me()
                .ok((data, message) => {
                    commit('setStudent', data);
                    result = true;
                })
                .unauthorized(async(data, message) => {
                    result = await dispatch('refresh');
                })
                .send();

            return result;
        },
        async refresh({ commit }) {
            //
        },
        async logout({ commit }) {
            await this._vm.$api.studentAuth.logout();
            commit('unsetTokens');
        },
    },

    mutations: {
        setStudent(state, student) {
            state.student = student
        },
        setTokens(state, { accessToken, refreshToken }) {
            state.accessToken = accessToken;
            this._vm.$axios.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`;
            localStorage.setItem(ACCESS_TOKEN_NAME, accessToken);
            localStorage.setItem(REFRESH_TOKEN_NAME, refreshToken);
        },
        unsetTokens(state) {
            state.accessToken = '';
            delete(this._vm.$axios.defaults.headers.common['Authorization']);
            localStorage.removeItem(ACCESS_TOKEN_NAME);
            localStorage.removeItem(REFRESH_TOKEN_NAME);
        }
    },

    namespaced: true,

}