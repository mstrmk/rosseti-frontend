import Vue from 'vue'
import Vuex from 'vuex'

import studentAuth from './modules/studentAuth'
import course from './modules/course'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        studentAuth,
        course,
    }
})