import Vue from "vue";
import Router from "vue-router";

// #errors pages
import PageNotFound from "@/views/errors/PageNotFound";

// #layouts
import Public from "@/views/layout/Public";
import StudentCabinet from "@/views/layout/StudentCabinet";

import Login from "@/views/public/Login";
import StudentCabinetHome from "@/views/studentCabinet/StudentCabinetHome";
import StudentCabinetCourses from "@/views/studentCabinet/StudentCabinetCourses";
import StudentCabinetCourse from "@/views/studentCabinet/StudentCabinetCourse";
import StudentCabinetLesson from "@/views/studentCabinet/StudentCabinetLesson";

// #middlewares
import studentAuth from "@/utils/middleware/studentAuth";

const mw = {
    check: { beforeEnter: studentAuth.check },
    auth: { beforeEnter: studentAuth.authenticated },
    notAuth: { beforeEnter: studentAuth.notAuthenticated },
};

// #helpers 
const meta = (title = '') => ({ meta: { title } });
const route = (path, component, name = null, title = '', middleware = {}) => ({
    path,
    component,
    name,
    ...meta(title),
    ...middleware,
});

// #router

Vue.use(Router);

export default new Router({
    mode: 'history',
    linkExactActiveClass: "active",
    routes: [
        { path: "*", component: PageNotFound }, // 404

        {
            path: "",
            component: Public,
            children: [{
                    path: "/",
                    redirect: { name: "studentCabinet.home" },
                },
                route("/login", Login, "login", 'Авторизация', mw.notAuth),
            ]
        },

        {
            path: "/student",
            component: StudentCabinet,
            ...mw.auth,
            children: [
                route("", StudentCabinetHome, "studentCabinet.home", 'Студенческий раздел'),
                route("courses", StudentCabinetCourses, "studentCabinet.courses", 'Доступные курсы'),
                route("courses/:id", StudentCabinetCourse, "studentCabinet.course", 'Курс'),
                route("courses/:courseId/:block.:lesson", StudentCabinetLesson, "studentCabinet.lesson", 'Урок'),
            ]
        },
    ],
    scrollBehavior: to => {
        if (to.hash) {
            return { selector: to.hash };
        } else {
            return { x: 0, y: 0 };
        }
    }
});