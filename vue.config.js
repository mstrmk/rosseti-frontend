const webpack = require('webpack');

module.exports = {
    configureWebpack: {
        // Set up all the aliases we use in our app.
        plugins: [
            new webpack.optimize.LimitChunkCountPlugin({
                maxChunks: 6
            })
        ],
        devServer: {
            disableHostCheck: true,
            port: 9001,
            public: 'rosseti.loc',
        }
    },
    pwa: {
        name: 'Rosseti',
        themeColor: '#172b4d',
        msTileColor: '#172b4d',
        appleMobileWebAppCapable: 'yes',
        appleMobileWebAppStatusBarStyle: '#172b4d'
    },
    css: {
        // Enable CSS source maps.
        sourceMap: process.env.NODE_ENV !== 'production',
        loaderOptions: {
            sass: {
                prependData: `@import "@/assets/scss/global/variables";`
            }
        }
    }
};